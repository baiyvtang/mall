package com.xiaobai.mall.cache;

import com.xiaobai.mall.cache.exception.CacheException;
import org.springframework.data.redis.core.RedisTemplate;

import java.util.Map;
import java.util.Set;

public class RedisCache implements ICache {

    private final RedisTemplate<String, Object> redisTemplate;

    public RedisCache(RedisTemplate<String, Object> redisTemplate) {
        this.redisTemplate = redisTemplate;
    }

    @Override
    public <T> Map<String, T> gets(String... key) throws CacheException {
        return null;
    }

    @Override
    public <T> T get(String key) throws CacheException {
        return (T) redisTemplate.opsForValue().get(key);
    }

    @Override
    public <T> T get(String key, long timeout) throws CacheException {
        return null;
    }

    @Override
    public <T> T getAndTouch(String key, int newExpireTime) throws CacheException {
        return null;
    }

    @Override
    public void incr(String key, Long value) throws CacheException {

    }

    @Override
    public void decr(String key, Long value) throws CacheException {

    }

    @Override
    public boolean set(String key, Object value) throws CacheException {
        redisTemplate.opsForValue().set(key, value);
        return true;
    }

    @Override
    public boolean set(String key, Object value, int exp) throws CacheException {
        return false;
    }

    @Override
    public boolean touch(String key, int newExpireTime) throws CacheException {
        return false;
    }

    @Override
    public boolean delete(String key) throws CacheException {
        return false;
    }

    @Override
    public boolean containKey(String key) throws CacheException {
        return false;
    }

    @Override
    public Set<String> keys() throws CacheException {
        return null;
    }

    @Override
    public boolean add(String key, Object value, int expireTime) throws CacheException {
        return false;
    }
}
