package com.xiaobai.mall.cache.exception;

public class CacheException extends Exception{
    public CacheException() {
        super();
    }

    public CacheException(String message) {
        super(message);
    }
}
