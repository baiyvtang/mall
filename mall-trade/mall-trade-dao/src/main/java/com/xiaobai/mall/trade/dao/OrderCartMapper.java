package com.xiaobai.mall.trade.dao;

import com.xiaobai.mall.trade.domain.OrderCart;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @author 小白
 */
@Mapper
public interface OrderCartMapper {

    List<OrderCart> queryOrderCartListByUserId(@Param("userId") String userId);

}