package com.xiaobai.mall.trade.dao;

import com.xiaobai.mall.trade.domain.Trade;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * @author 小白
 */
@Mapper
public interface TradeMapper {

    List<Trade> queryList();
}