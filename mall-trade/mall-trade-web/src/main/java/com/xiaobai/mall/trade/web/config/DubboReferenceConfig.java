package com.xiaobai.mall.trade.web.config;

import com.xiaobai.mall.item.interfaces.IItemService;
import org.apache.dubbo.config.annotation.DubboReference;
import org.springframework.context.annotation.Configuration;

@Configuration
public class DubboReferenceConfig {
    @DubboReference(timeout = 3000)
    private IItemService itemService;
}
