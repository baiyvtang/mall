package com.xiaobai.mall.trade.web.controller;

import com.xiaobai.mall.item.domain.Item;
import com.xiaobai.mall.item.interfaces.IItemService;
import com.xiaobai.mall.trade.domain.Trade;
import com.xiaobai.mall.trade.interfaces.ITradeService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;

@RestController
@RequestMapping("/trade")
public class TradeController {

    @Resource
    private ITradeService tradeService;

    @Resource
    private IItemService itemService;

    @GetMapping("/item/list")
    public List<Item> queryItemList() {
        return itemService.queryList();
    }

    @GetMapping("/list")
    public List<Trade> queryList() {
        return tradeService.queryList();
    }
}
