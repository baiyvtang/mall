package com.xiaobai.mall.trade.web.controller;

import com.xiaobai.mall.trade.domain.OrderCart;
import com.xiaobai.mall.trade.interfaces.IOrderCartService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;

@RestController
@RequestMapping("OrderCart")
public class OrderCartController {

    @Resource
    private IOrderCartService orderCartService;

    @GetMapping("queryOrderCartListByUserId")
    public List<OrderCart> queryOrderCartListByUserId(@RequestParam String userId) {
        return orderCartService.queryOrderCartListByUserId(userId);
    }

}
