package com.xiaobai.mall.trade.web;

import org.apache.dubbo.config.spring.context.annotation.EnableDubbo;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan(basePackages = "com.xiaobai.mall.trade")
@MapperScan("com.xiaobai.mall.trade.dao")
@EnableDubbo
public class MallTradeWebSpringbootApplication {
    public static void main(String[] args) {
        SpringApplication.run(MallTradeWebSpringbootApplication.class, args);
    }
}
