package com.xiaobai.mall.trade.interfaces;

import com.xiaobai.mall.trade.domain.Trade;

import java.util.List;

public interface ITradeService {

    List<Trade> queryList();
}
