package com.xiaobai.mall.trade.interfaces;

import com.xiaobai.mall.trade.domain.OrderCart;
import org.springframework.stereotype.Service;

import java.util.List;

public interface IOrderCartService {

    List<OrderCart> queryOrderCartListByUserId(String userId);
}
