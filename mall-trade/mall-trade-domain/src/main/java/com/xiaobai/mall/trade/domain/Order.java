package com.xiaobai.mall.trade.domain;

import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;

@Data
public class Order {

    private Long id;

    /**
     * 订单表id
     */
    private Integer tradeId;

    /**
     * 订单商品id
     */
    private Integer itemId;

    /**
     * 商品名称
     */
    private String itemTitle;

    /**
     * 购买商品数量
     */
    private Integer num;

    /**
     * 购买商品单价
     */
    private BigDecimal itemPrice;

    /**
     * 商品重量
     */
    private Float weight;

    /**
     * 优惠分摊金额
     */
    private BigDecimal feeMoney;

    /**
     * 创建时间
     */
    private Date createTime;

    /**
     * 最后一次修改时间
     */
    private Date updateTime;

}