package com.xiaobai.mall.trade.service;

import com.xiaobai.mall.trade.dao.TradeMapper;
import com.xiaobai.mall.trade.domain.Trade;
import com.xiaobai.mall.trade.interfaces.ITradeService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service
public class TradeService implements ITradeService {

    @Resource
    private TradeMapper tradeMapper;


    @Override
    public List<Trade> queryList() {
        return tradeMapper.queryList();
    }
}
