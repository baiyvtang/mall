package com.xiaobai.mall.trade.service;

import com.xiaobai.mall.trade.dao.OrderCartMapper;
import com.xiaobai.mall.trade.domain.OrderCart;
import com.xiaobai.mall.trade.interfaces.IOrderCartService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;
@Service
public class OrderCartServiceImpl implements IOrderCartService {

    @Resource
    private OrderCartMapper orderCartMapper;

    @Override
    public List<OrderCart> queryOrderCartListByUserId(String userId) {
        return orderCartMapper.queryOrderCartListByUserId(userId);
    }
}
