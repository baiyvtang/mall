package com.xiaobai.mall.index.dao;

import com.xiaobai.mall.index.domain.User;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface UserMapper {

    User queryByUsername(String username);
}
