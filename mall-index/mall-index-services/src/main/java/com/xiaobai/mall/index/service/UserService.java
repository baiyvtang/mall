package com.xiaobai.mall.index.service;

import com.xiaobai.mall.index.api.IUserService;
import com.xiaobai.mall.index.dao.UserMapper;
import com.xiaobai.mall.index.domain.User;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

@Service
public class UserService implements IUserService {

    @Resource
    UserMapper userMapper;

    @Override
    public User queryUserByName(String username) {
        return userMapper.queryByUsername(username);
    }
}
