package com.xiaobai.mall.index.web;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication(scanBasePackages = "com.xiaobai.mall.index")
@MapperScan("com.xiaobai.mall.index.dao")
public class MallIndexWebSpringbootApplication {

    public static void main(String[] args) {
        SpringApplication.run(MallIndexWebSpringbootApplication.class, args);
    }
}
