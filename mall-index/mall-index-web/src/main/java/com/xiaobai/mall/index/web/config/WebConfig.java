package com.xiaobai.mall.index.web.config;

import com.xiaobai.mall.index.web.interceptor.SessionInterceptor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
public class WebConfig implements WebMvcConfigurer {
    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(handlerInterceptor()).excludePathPatterns("/account/login");
    }
    @Bean
    HandlerInterceptor handlerInterceptor() {
        return new SessionInterceptor();
    }
}
