package com.xiaobai.mall.index.api;

import com.xiaobai.mall.index.domain.User;

public interface IUserService {

    User queryUserByName(String username);
}
