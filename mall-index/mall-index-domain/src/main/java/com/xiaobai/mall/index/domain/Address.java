package com.xiaobai.mall.index.domain;

import lombok.Data;

import java.util.Date;

@Data
public class Address {


    private Long id;

    /**
     * 用户id
     */
    private Long userId;

    /**
     * 省份
     */
    private String province;

    /**
     * 城市
     */
    private String city;

    /**
     * 区
     */
    private String district;

    /**
     * 详细地址
     */
    private String address;

    /**
     * 状态，1代表启用，0代表删除
     */
    private int enableStatus;

    /**
     * 创建时间
     */
    private Date createTime;

    /**
     * 最后一次修改时间
     */
    private Date updateTime;

}