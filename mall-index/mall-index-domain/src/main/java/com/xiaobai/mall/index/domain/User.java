package com.xiaobai.mall.index.domain;

import lombok.Data;

import java.util.Date;

@Data
public class User {


    private Long id;

    /**
     * 账号
     */
    private String account;

    /**
     * 昵称
     */
    private String nick;

    /**
     * 密码
     */
    private String password;

    /**
     * 头像
     */
    private String photo;

    /**
     * 创建时间
     */
    private Date createTime;

    /**
     * 最后一次修改时间
     */
    private Date updateTime;
}