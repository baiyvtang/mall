package com.xiaobai.mall.item.interfaces;

import com.xiaobai.mall.item.domain.Item;
import com.xiaobai.mall.item.dto.ItemDTO;
import com.xiaobai.mall.item.param.ItemQueryParam;

import java.util.List;

public interface IItemService {

    List<Item> queryList();

    List<Item> queryListByParam(ItemQueryParam param);

    Item queryItemByOuterId(String outerId);


    Item queryItemById(Long id);

    void deleteItem(Long id);

    Item createItem(ItemDTO itemDTO);

    Item updateItem(ItemDTO itemDTO);

    void updatePublishStatus(Long id, Integer publishStatus);
}
