package com.xiaobai.mall.item.param;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class BaseParam {

    private Integer pageNum = 1;

    private Integer pageSize = 20;

}
