package com.xiaobai.mall.item.dto;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class ItemDTO {

    /**
     * 商家编码
     */
    private String outerId;

    /**
     * 商品名称
     */
    private String title;

    /**
     * 类目id
     */
    private Integer categoryId;

    /**
     * 商品详情
     */
    private String description;

    /**
     * 商品主图列表
     */
    private String mainImages;

    /**
     * 商品销售价格
     */
    private BigDecimal price;

    /**
     * 商品重量
     */
    private Float weight;

    /**
     * 商品长度
     */
    private Float length;

    /**
     * 商品高度
     */
    private Float height;

    /**
     * 商品宽度
     */
    private Float width;

    /**
     * 上下架状态：0下架1上架
     */
    private boolean publishStatus;
}
