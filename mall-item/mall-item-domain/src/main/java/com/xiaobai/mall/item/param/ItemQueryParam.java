package com.xiaobai.mall.item.param;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
public class ItemQueryParam extends BaseParam {

    private String title;

    private String outerId;

    private String sortField;

    private Boolean sortAsc;

    /**
     * 上下架状态
     */
    private Integer publishStatus;

    /**
     * 启用状态
     */
    private Integer enableStatus;

}
