package com.xiaobai.mall.item.domain;

import lombok.Data;

import java.util.Date;

/**
 * @author 小白
 */
@Data
public class ItemCategory {

    private Long id;

    /**
     * 分类名称
     */
    private String categoryName;

    /**
     * 分类编码
     */
    private String categoryCode;

    /**
     * 父分类id
     */
    private Integer parentId;

    /**
     * 分类层级
     */
    private boolean categoryLevel;

    /**
     * 分类状态
     */
    private boolean categoryStatus;

    /**
     * 最后修改时间
     */
    private Date modifiedTime;

    /**
     * 创建时间
     */
    private Date createTime;

    /**
     * 最后一次修改时间
     */
    private Date updateTime;


}