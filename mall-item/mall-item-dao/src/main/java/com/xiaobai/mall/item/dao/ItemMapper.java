package com.xiaobai.mall.item.dao;

import com.xiaobai.mall.item.domain.Item;
import com.xiaobai.mall.item.param.ItemQueryParam;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * @author 小白
 */
@Mapper
public interface ItemMapper {

    List<Item> queryList(ItemQueryParam param);

    Boolean saveItem(Item target);

    Item queryById(Long id);

    void deleteItem(Long id);
}