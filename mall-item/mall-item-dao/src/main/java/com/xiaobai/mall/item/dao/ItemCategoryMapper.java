package com.xiaobai.mall.item.dao;

import com.xiaobai.mall.item.domain.ItemCategory;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * @author 小白
 * @description item_category
 * @date 2023-05-31
 */
@Mapper
public interface ItemCategoryMapper {

    /**
     * 新增
     *
     * @author 小白
     * @date 2023/05/31
     **/
    int insert(ItemCategory itemCategory);

    /**
     * 刪除
     *
     * @author 小白
     * @date 2023/05/31
     **/
    int delete(int id);

    /**
     * 更新
     *
     * @author 小白
     * @date 2023/05/31
     **/
    int update(ItemCategory itemCategory);

    /**
     * 查询 根据主键 id 查询
     *
     * @author 小白
     * @date 2023/05/31
     **/
    ItemCategory load(int id);

    /**
     * 查询 分页查询
     *
     * @author 小白
     * @date 2023/05/31
     **/
    List<ItemCategory> pageList(int offset, int pagesize);

    /**
     * 查询 分页查询 count
     *
     * @author 小白
     * @date 2023/05/31
     **/
    int pageListCount(int offset, int pagesize);

}