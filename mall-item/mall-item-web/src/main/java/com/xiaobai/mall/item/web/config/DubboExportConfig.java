package com.xiaobai.mall.item.web.config;

import com.xiaobai.mall.item.interfaces.IItemService;
import org.apache.dubbo.config.spring.ServiceBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class DubboExportConfig {

    @Bean
    public ServiceBean<IItemService> itemServiceConfig(IItemService itemService) {
        ServiceBean<IItemService> serviceBean = new ServiceBean<>();
        serviceBean.setInterface(IItemService.class);
        serviceBean.setRef(itemService);
        return serviceBean;
    }
}
