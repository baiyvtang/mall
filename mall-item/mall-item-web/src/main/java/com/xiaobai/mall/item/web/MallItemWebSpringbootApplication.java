package com.xiaobai.mall.item.web;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan(basePackages = "com.xiaobai.mall.item")
@MapperScan("com.xiaobai.mall.item.dao")
//@EnableDubbo
public class MallItemWebSpringbootApplication {
    public static void main(String[] args) {
        SpringApplication.run(MallItemWebSpringbootApplication.class, args);
    }
}
