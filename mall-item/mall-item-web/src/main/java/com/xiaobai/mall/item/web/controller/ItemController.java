package com.xiaobai.mall.item.web.controller;

import com.xiaobai.mall.item.domain.Item;
import com.xiaobai.mall.item.dto.ItemDTO;
import com.xiaobai.mall.item.interfaces.IItemService;
import com.xiaobai.mall.item.param.ItemQueryParam;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

@RestController
@RequestMapping("/item")
public class ItemController {

    @Resource
    private IItemService itemService;

    @GetMapping("/list")
    public List<Item> queryList(ItemQueryParam param) {
        return itemService.queryListByParam(param);
    }


    @PostMapping("queryItemList")
    public List<Item> queryItemList(@RequestBody ItemQueryParam param) {
        return itemService.queryListByParam(param);
    }

    @PostMapping("/add")
    public Item createItem(@RequestBody ItemDTO ItemDTO) {
        return itemService.createItem(ItemDTO);
    }

    @GetMapping("/update")
    public Item updateItem(@RequestBody ItemDTO ItemDTO) {
        return itemService.updateItem(ItemDTO);
    }

    @GetMapping("/delete/{id}")
    public void deleteItem(@PathVariable Long id) {
        itemService.deleteItem(id);
    }

    @GetMapping("/updatePublishStatus")
    public void updatePublishStatus(Long id, Integer publishStatus) {
        itemService.updatePublishStatus(id, publishStatus);
    }
}
