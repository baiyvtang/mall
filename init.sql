create database mall;
use mall;
create table user
(
    id            bigint primary key auto_increment,
    account       varchar(15) not null comment '账号',
    nick          varchar(15) comment '昵称',
    password      char(32) comment '密码',
    photo         varchar(150) comment '头像',
    `create_time` timestamp   NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
    `update_time` timestamp   NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '最后一次修改时间'
) engine = innodb
  character set = utf8;
insert into user(account, nick, password, photo)
values ('admin', '白玉汤', '123456', null);
create table address
(
    id            bigint primary key auto_increment,
    user_id       bigint      not null comment '用户id',
    province      varchar(10) not null comment '省份',
    city          varchar(10) not null comment '城市',
    district      varchar(20) not null comment '区',
    address       varchar(30) not null comment '详细地址',
    is_primary    tinyint     not null default 0,
    enable_status tinyint              default 1 comment '状态，1代表启用，0代表删除',
    `create_time` timestamp   NULL     DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
    `update_time` timestamp   NULL     DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '最后一次修改时间',
    key INDEX_USER_ID (`user_id`)
) engine = innodb
  character set = utf8;
insert into address (user_id, province, city, district, address, is_primary, enable_status) VALUE (1, '安徽省', '淮北市', '濉溪县', '地址', 1, 1);
CREATE TABLE item_category
(
    id              int UNSIGNED AUTO_INCREMENT COMMENT '分类ID' primary key,
    category_name   VARCHAR(10)       NOT NULL COMMENT '分类名称',
    category_code   VARCHAR(10)       NOT NULL COMMENT '分类编码',
    parent_id       SMALLINT UNSIGNED NOT NULL DEFAULT 0 COMMENT '父分类ID',
    category_level  TINYINT           NOT NULL DEFAULT 1 COMMENT '分类层级',
    category_status TINYINT           NOT NULL DEFAULT 1 COMMENT '分类状态',
    `create_time`   timestamp         NULL     DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
    `update_time`   timestamp         NULL     DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '最后一次修改时间'
) ENGINE = innodb COMMENT '商品分类表';
insert into item_category (category_name, category_code, parent_id, category_level,
                           category_status) VALUE ('女装', '女装', 0, 1, 1);
create table item
(
    id             bigint primary key auto_increment,
    outer_id       varchar(20)   not null comment '商家编码',
    title          varchar(20)   not null comment '商品名称',
    category_id    int           not null comment '类目id',
    description    text          not null comment '商品详情',
    main_images    text          not null comment '商品主图列表',
    price          DECIMAL(8, 2) NOT NULL COMMENT '商品销售价格',
    weight         FLOAT COMMENT '商品重量',
    length         FLOAT COMMENT '商品长度',
    height         FLOAT COMMENT '商品高度',
    width          FLOAT COMMENT '商品宽度',
    publish_status TINYINT       NOT NULL DEFAULT 0 COMMENT '上下架状态：0下架1上架',
    enable_status  TINYINT       NOT NULL DEFAULT 1 COMMENT '启动状态，0删除1正常',
    `create_time`  timestamp     NULL     DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
    `update_time`  timestamp     NULL     DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '最后一次修改时间',
    unique key index_outer_id (`outer_id`)
);
insert into item(outer_id, title, category_id, description, main_images, price, weight, length, height, width,
                 publish_status) VALUE (
                                        'test商品编码1', 'test商品名称1', 1, '商品描述', '', 12, 1, null, null, null, 1
    );
insert into item(outer_id, title, category_id, description, main_images, price, weight, length, height, width,
                 publish_status) VALUE (
                                        'test商品编码2', 'test商品编码1', 1, '商品描述', '', 12, 1, null, null, null, 1
    );


CREATE TABLE `trade`
(
    id                 bigint          NOT NULL AUTO_INCREMENT COMMENT '订单ID' primary key,
    trade_sn           BIGINT UNSIGNED NOT NULL COMMENT '订单编号',
    user_id            INT UNSIGNED    NOT NULL COMMENT '下单人ID',
    user_nick          VARCHAR(10)     NOT NULL COMMENT '收货人姓名',
    province           SMALLINT        NOT NULL COMMENT '省',
    city               SMALLINT        NOT NULL COMMENT '市',
    district           SMALLINT        NOT NULL COMMENT '区',
    address            VARCHAR(100)    NOT NULL COMMENT '地址',
    payment_method     TINYINT         NOT NULL COMMENT '支付方式：1现金，2余额，3网银，4支付宝，5微信',
    order_money        DECIMAL(8, 2)   NOT NULL COMMENT '订单金额',
    district_money     DECIMAL(8, 2)   NOT NULL DEFAULT 0.00 COMMENT '优惠金额',
    shipping_money     DECIMAL(8, 2)   NOT NULL DEFAULT 0.00 COMMENT '运费金额',
    payment_money      DECIMAL(8, 2)   NOT NULL DEFAULT 0.00 COMMENT '支付金额',
    shipping_comp_name VARCHAR(10) COMMENT '快递公司名称',
    shipping_sn        VARCHAR(50) COMMENT '快递单号',
    shipping_time      DATETIME COMMENT '发货时间',
    pay_time           DATETIME COMMENT '支付时间',
    receive_time       DATETIME COMMENT '收货时间',
    `create_time`      timestamp       NULL     DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
    `update_time`      timestamp       NULL     DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '最后一次修改时间'
) ENGINE = innodb COMMENT '订单表';

CREATE TABLE `order`
(
    id            bigint        NOT NULL AUTO_INCREMENT COMMENT '订单详情表ID' primary key,
    trade_id      INT UNSIGNED  NOT NULL COMMENT '订单表ID',
    item_id       INT UNSIGNED  NOT NULL COMMENT '订单商品ID',
    item_title    VARCHAR(20)   NOT NULL COMMENT '商品名称',
    num           INT           NOT NULL DEFAULT 1 COMMENT '购买商品数量',
    item_price    DECIMAL(8, 2) NOT NULL COMMENT '购买商品单价',
    weight        FLOAT COMMENT '商品重量',
    fee_money     DECIMAL(8, 2) NOT NULL DEFAULT 0.00 COMMENT '优惠分摊金额',
    `create_time` timestamp     NULL     DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
    `update_time` timestamp     NULL     DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '最后一次修改时间'
) ENGINE = innodb COMMENT '订单详情表';
CREATE TABLE order_cart
(
    id            INT UNSIGNED  NOT NULL AUTO_INCREMENT COMMENT '购物车ID' primary key,
    user_id       INT UNSIGNED  NOT NULL COMMENT '用户ID',
    item_id       INT UNSIGNED  NOT NULL COMMENT '商品ID',
    item_num      INT           NOT NULL COMMENT '加入购物车商品数量',
    price         DECIMAL(8, 2) NOT NULL COMMENT '商品价格',
    `create_time` timestamp     NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
    `update_time` timestamp     NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '最后一次修改时间'
) ENGINE = innodb COMMENT '购物车表';