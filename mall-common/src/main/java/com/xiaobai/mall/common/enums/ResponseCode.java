package com.xiaobai.mall.common.enums;

import lombok.Getter;

@Getter
public enum ResponseCode {

    SUCCESS(2000, "成功"),

    REQUEST_ERROR(4000, "请求错误"),

    VALIDATE_ERROR(4022, "参数错误"),

    SESSION_ERROR(4055, "鉴权错误"),

    BUSINESS_ERROR(5001, "服务器内部错误");

    private final Integer code;

    private final String desc;

    ResponseCode(Integer code, String desc) {
        this.code = code;
        this.desc = desc;
    }
}
