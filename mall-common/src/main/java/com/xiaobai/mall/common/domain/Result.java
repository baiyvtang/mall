package com.xiaobai.mall.common.domain;

import cn.hutool.core.util.StrUtil;
import com.xiaobai.mall.common.enums.ResponseCode;
import lombok.Data;

@Data
public class Result<T> {

    private Integer code;

    private Boolean success;

    private T data;

    private String errorMsg;

    public static <T> Result<T> buildSuccessResult(T data) {
        Result<T> result = new Result<>();
        result.setSuccess(Boolean.TRUE);
        result.setCode(ResponseCode.SUCCESS.getCode());
        result.setData(data);
        return result;
    }

    public static <T> Result<T> buildErrorResult(ResponseCode code, String errorMsg) {
        StringBuilder error = new StringBuilder();
        error.append(code.getDesc());
        if (!StrUtil.isBlank(errorMsg)) {
            error.append(": ").append(errorMsg);
        }
        Result<T> result = new Result<>();
        result.setSuccess(Boolean.FALSE);
        result.setCode(code.getCode());
        result.setErrorMsg(error.toString());
        return result;
    }

}
