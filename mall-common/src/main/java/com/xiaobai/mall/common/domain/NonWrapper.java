package com.xiaobai.mall.common.domain;


import java.lang.annotation.*;

/**
 * controller层加上这个注解禁止包装返回值
 */
@Target({ElementType.TYPE, ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface NonWrapper {
}
