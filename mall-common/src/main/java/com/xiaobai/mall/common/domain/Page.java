package com.xiaobai.mall.common.domain;

import lombok.Data;

@Data
public class Page {

    private int page;

    private int pageSize = 20;
}
