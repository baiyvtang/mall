package com.xiaobai.mall.common.exception;

public class SessionException extends RuntimeException {
    public SessionException(String message) {
        super(message);
    }

    public SessionException() {
    }

    @Override
    public String getMessage() {
        return super.getMessage();
    }
}
